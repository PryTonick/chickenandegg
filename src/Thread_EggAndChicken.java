public class Thread_EggAndChicken extends Thread {
    private String name;
    private int count;
    private Thread_EggAndChicken oponent;

    public Thread_EggAndChicken(String name) {
        this.name = name;
    }

    public void setOpponent(Thread_EggAndChicken opponent) {
        oponent = opponent;
    }

    public void run() {
        while((count++) < 5_000) {
            System.out.println(name);
        }

        if(!oponent.isAlive()) {
            System.out.printf("Побеждает %s", name);
        }
    }
}
