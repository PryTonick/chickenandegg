/**
 * Задание: Написать 2 потока ( Яйцо и Курица ), которые будут спорить между собой.
 * Кто первый появился.
 *
 * @author Gorshkov Nikita , 16IT18K
 */
public class Main {
    public static void main(String[] args) {
        Thread_EggAndChicken chicken = new Thread_EggAndChicken("Курица");
        Thread_EggAndChicken egg = new Thread_EggAndChicken("Яйцо");

        chicken.setOpponent(egg);
        egg.setOpponent(chicken);

        chicken.start();
        egg.start();

        try {
            chicken.join();
            egg.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
